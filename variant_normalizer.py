#!/usr/bin/env python

from pyfaidx import Fasta

class Normalizer:
    def __init__(self, fastaFile):
        self.reference = Fasta(fastaFile)

    def normalize(self, contig, pos, ref, *alts):
        return normalize(self.reference, contig, pos, ref, *alts)

def normalize(reference, contig, pos, ref, *alts):
    """Left-shift and minimize variant representations.

    Arguments:
    reference -- a pyfaidx Fasta object (provides reference sequence)
    contig -- sequence identifier (e.g., chromosome)
    pos -- one-based start position of the ref/alts
    alts -- one or more alternate bases

    Algorithm is modified from Tan et al https://doi.org/10.1093/bioinformatics/btv112

        Algorithm 1 Normalize a VCF entry
        Input: A VCF entry and the reference genome sequence.
        Output: A normalized VCF entry

        1 : do
        2 :   if all alleles end with same nucleotide then
        3 :     truncate the rightmost nucleotide of each allele
        4 :   if any allele is length zero then
        5 :     extend all alleles by 1 nucleotide to the left
        6 : while changes made in the VCF entry in the loop
        7 : while all alleles start with same nucleotide and length >=2 do
        8 :   truncate the leftmost nucleotide of each allele
        9 : end while
        10 : return the VCF entry

    Note: The algorithm is altered slightly from above (moving 4,5 before 2,3),
    so you can pass in an empty ref or alt string.
    """

    pos = int(pos)
    # special case: single alt allele that is same as reference:
    if len(alts) == 1 and ref == alts[0]:
        return (pos, ref, alts)

    alts = ['' if x == '-' else x for x in alts]
    if ref == '-':
        ref = ''
        pos += 1
    done = False
    while not done:
        done = True
        if '' == ref or '' in alts:
            done = False
            pos -= 1
            prior = reference[contig][pos-1].seq.upper()
            ref = str(prior + ref)
            alts = [str(prior + a) for a in alts]
        if all( (a[-1] == ref[-1] for a in alts) ):
            done = False
            ref = ref[:-1]
            alts = [a[:-1] for a in alts]

    while len(ref) >= 2 and all( (len(a) >= 2 and a[0] == ref[0] for a in alts) ):
        pos += 1
        ref = ref[1:]
        alts = [a[1:] for a in alts]

    return (pos, ref, tuple(alts))
