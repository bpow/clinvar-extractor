Extracts info from the ClinVarFullRelease xml file.

Mostly written in order to have a specific aggregation of SCVs to try to balance sensitivity and specificity.

More concretely, the script excludes from consideration thos SCVs with `scv_review_status` that is empty,
'no assertion provided', 'no assertion criteria provided' or 'criteria provided, conflicting interpretations'
(the last one being weird for an SCV, but whatever...). These are available in the `scv_with_criteria field`.
Then, the field `clinvar_asserted_pathogenic` is assigned a value of 1 (representing "true") if aI
least one submitter reported an interpretation of pathogenic or likely pathgenic using established criteria.


Requirements:
- Python (I've used 2.7 and 3.6)
- Python modules (listed in requirements.txt, so you can `pip install -r requirements.txt`
  - lxml
  - PyYAML (only needed if you want YAML output
  - pyfaidx (to look up reference bases in fasta file for variant normalization)
- an appropriate reference genome fasta file, by default code looks for one of:
  - [GCF_000001405.26_GRCh38_genomic.fna](https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.26_GRCh38/GCF_000001405.26_GRCh38_genomic.fna.gz) or
  - [GCF_000001405.25_GRCh37.p13_genomic.fna](https://ftp.ncbi.nih.gov/genomes/refseq/vertebrate_mammalian/Homo_sapiens/all_assembly_versions/GCF_000001405.25_GRCh37.p13/GCF_000001405.25_GRCh37.p13_genomic.fna.gz)
  - *note* that these files must be gunzipped after downloading

The input xml is streamed using lxml.etree and parsing at the clinvarset level, so memory requirements are trim (probably less than 100 MB)

Option help is available using the `--help` options. That will also tell you about the other exciting output formats you can try.

# Example

```bash
# setup virtualenv (only need to do this once after install):
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt # if you do not have libxml2 and python development headers installed, you could try using conda instead

# download and uncompress reference genome (also only need to do once, but get the GRCh37 version if you want that)
curl https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.26_GRCh38/GCF_000001405.26_GRCh38_genomic.fna.gz | gzip -dc > GCF_000001405.26_GRCh38_genomic.fna

VERSION=2018-07
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/xml/ClinVarFullRelease_${VERSION}.xml.gz

python extract_scv_info.py -a GRCh38 -i ClinVarFullRelease_${VERSION}.xml.gz -f vcf | bgzip -c > ClinVar.GRCh38.${VERSION}.vcf.gz; tabix -p vcf ClinVar.GRCh38.${VERSION}.vcf.gz
```
