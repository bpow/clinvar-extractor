#!/usr/bin/env python

from __future__ import print_function

import sys
import argparse
import gzip
import json
from xml_iterate import iterate_by_tag
from variant_normalizer import Normalizer
import collections
import lxml.etree as etree
import logging
import urllib

VERSION='0.0.1'

log = logging.getLogger('clinvar-extractor')
log.addHandler(logging.StreamHandler())

included_scv_review_statuses = (
    'reviewed by expert panel',
    'practice guideline',
    'criteria provided, single submitter')

excluded_scv_review_statuses = (
    'criteria provided, conflicting interpretations', # that's just plain weird for an SCV...
    'no assertion criteria provided',
    'no assertion provided',
    ''
)

Variant = collections.namedtuple('Variant', 'contig pos ref alt')
Variant.__str__ = lambda v: '|'.join((str(x) for x in v))

fastas = {
    'GRCh37': 'GCF_000001405.25_GRCh37.p13_genomic.fna',
    'GRCh38': 'GCF_000001405.26_GRCh38_genomic.fna'
}


def variant_from_fields(loc, normalizers, accession, position, ref, alt):
    v = Variant(*(loc.get(x) for x in (accession, position, ref, alt)))
    if any (x is None for x in v):
        return None
    normalizer = normalizers.get(loc.get('Assembly'))
    if normalizer is None:
        log.warning("Unable to find normalizer for assembly [%s] location %s", loc.get("Assembly"), str(loc))
        return v
    pos, ref, alts = normalizer.normalize(*v)
    return Variant(v.contig, pos, ref, alts[0])


def process_sequence_location(loc, normalizers):
    #parentType = loc.getparent().get("Type")
    vcf_variant = variant_from_fields(loc, normalizers, 'Accession', 'positionVCF', 'referenceAlleleVCF', 'alternateAlleleVCF')
    old_style_variant = variant_from_fields(loc, normalizers, 'Accession', 'start', 'referenceAllele', 'alternateAllele')

    if old_style_variant and vcf_variant and old_style_variant != vcf_variant:
        log.warning("both vcf_variant and old_style_variant provided, but do not normalize to the same!\n  %s\n  %s != %s",
                  (str(loc), vcf_variant, old_style_variant))
        return None
    if vcf_variant:
        return vcf_variant
    elif old_style_variant:
        log.info("No vcf variant, so used %s", str(old_style_variant))
        return old_style_variant
    else:
        log.warning("Not enough info to process location: %s", str(loc))
        return None

#
# a few wrapper functions to decorate around compiled XPath queries...
#

def xpath_one_string(query):
    evaluator = etree.XPath(query)
    def wrapper(element, *args, **kwargs):
        res = evaluator(element, *args, **kwargs)
        if isinstance(res, list):
            if len(res) == 0:
                res = ''
            elif len(res) > 1:
                log.error('Too many items for %s in %s', evaluator.path, etree.tostring(*args[0]))
                res = res[0]
            else:
                res = res[0]
        return res
    return wrapper


def xpath_multi_string(query):
    evaluator = etree.XPath(query)
    def wrapper(element, *args, **kwargs):
        return [x for x in evaluator(element, *args, **kwargs)]
    return wrapper


# FIXME: probably something in standard library to do this?
def elem_attr(query, *args):
    def wrapper(element):
        return element.get(query, *args)
    return wrapper


#
# The various queries used to find the stuff we are interested in...
#

def find_locations(clinvarset):
    return clinvarset.xpath('ReferenceClinVarAssertion/MeasureSet[@Type="Variant"]/Measure/SequenceLocation')

def find_measure_relationships(clinvarset):
    return clinvarset.xpath('ReferenceClinVarAssertion/MeasureSet[@Type="Variant"]/Measure/MeasureRelationship')

def find_scvs(clinvarset):
    return clinvarset.findall('ClinVarAssertion')

# we need this to get at cases where ClinVar reinterprets the submitted significance
def scv_comments(scv):
    return xpath_multi_string('ClinicalSignificance/Comment/text( )')


ItemSelection = collections.namedtuple('ItemSelection', 'name selector number description')

varset_selectors = collections.OrderedDict(((x.name, x) for x in [
    ItemSelection('clinvar_assertion_id', xpath_multi_string('ClinVarAssertion/@ID'), '1', "ClinVar ID of Assertion"),
    ItemSelection('clinvarset_title', xpath_one_string('Title/text( )'), '1', "ClinVar title/summary for ClinVarSet"),
    ItemSelection('clinvarset_record_status', xpath_one_string('RecordStatus/text( )'), '1', "Status of ClinVarSet record"),
    ItemSelection('clinvar_asserted_pathogenic', lambda x: '', '1', "Whether at least one submitter using criteria asserted this variant as pathogenic or likely pathogenic"),
    ItemSelection('scv_with_criteria', lambda x: '', '1', 'List of assertions using criteria (SCV_accession|assertion)'),
    ItemSelection('rcv_accession', xpath_one_string('concat(ReferenceClinVarAssertion/ClinVarAccession/@Acc, ".", ReferenceClinVarAssertion/ClinVarAccession/@Version)'), '1', 'ClinVar RCV accession'),
    ItemSelection('rcv_significance', xpath_one_string('ReferenceClinVarAssertion/ClinicalSignificance/Description/text( )'), '1', 'Clinical significance for the RCV as aggregated by ClinVar'),
    ItemSelection('rcv_explanation', xpath_one_string('ReferenceClinVarAssertion/ClinicalSignificance/Explanation/text( )'), '1', 'Explanation for aggregated clinical significance by ClinVar'),
    ItemSelection('rcv_traits', xpath_multi_string('ReferenceClinVarAssertion/TraitSet[@Type="Disease"]/Trait[@Type="Disease"]/Name/ElementValue[@Type="Preferred"]/text( )'), '1', 'Condition associated with assertion'),
    # FIXME - should "Number" of clinvar_variation_id be '1'?
    ItemSelection('clinvar_variation_id', xpath_one_string('ReferenceClinVarAssertion/MeasureSet[@Type="Variant"]/@ID'), '1', 'ClinVar VariationID'),
    # ItemSelection('rcv_review_status', xpath_one_string('ReferenceClinVarAssertion/ClinicalSignificance/ReviewStatus/text( )'), '1', 'Review status for ClinVar RCV record'),
    # ItemSelection('rcv_date_created', xpath_one_string('ReferenceClinVarAssertion/@DateCreated'), '1', 'Date of creation of RCV record'),
    # ItemSelection('rcv_date_updated', xpath_one_string('ReferenceClinVarAssertion/@DateLastUpdated', '1', 'Last update date of RCV record')),
    # ItemSelection('rcv_date_last_evaluated', xpath_one_string('ReferenceClinVarAssertion/ClinicalSignificance/@DateLastEvaluated', '1', 'Last evaluation date of RCV record'))
]))

scv_selectors = collections.OrderedDict(((x.name, x) for x in [
    ItemSelection('accession', xpath_one_string('concat(ClinVarAccession/@Acc, ".", ClinVarAccession/@Version)'), '1', 'ClinVar SCV accession'),
    ItemSelection('submitter', xpath_one_string('ClinVarSubmissionID/@submitter'), '1', 'Submitter of ClinVar SCV'),
    ItemSelection('date_updated', xpath_one_string('ClinVarAccession/@DateUpdated'), '1', 'Last update for SCV record'),
    ItemSelection('significance', xpath_one_string('ClinicalSignificance/Description/text( )'), '1', 'ClinVar significance (as used for aggregation)'),
    ItemSelection('original_significance', lambda x: '', '1', 'Original significance of SCV record (if ClinVar interprets the significance differently for aggregation)'), # to fill out later, if comment indicates this was changed
    ItemSelection('significance_comment', xpath_multi_string('ClinicalSignificance/Comment/text( )'), '1', 'Comments to the SCV asserted significance'),
    ItemSelection('review_status', xpath_one_string('ClinicalSignificance/ReviewStatus/text( )'), '1', 'Review status of ClinVar SCV record'),
    ItemSelection('disease', xpath_multi_string('TraitSet[@Type="Disease"]/Trait[@Type="Disease"]/Name/ElementValue[@Type="Preferred"]/text( )'), '1', 'Condition to which assertion is made'),
]))

exclude_from_vcf_scv = ['significance_comment']

scv_info_header = '##INFO=<ID=clinvar_scv,Number=1,Type=String,Description="ClinVar SCV info, with fields delimited by pipe characters: (%s)">'%(
    '|'.join((name for name in scv_selectors if name not in exclude_from_vcf_scv)),)

location_selectors = collections.OrderedDict([(attrib, elem_attr(attrib)) for attrib in (
    'Assembly', 'Accession', 'positionVCF', 'referenceAlleleVCF', 'alternateAlleleVCF', 'start', 'referenceAllele', 'alternateAllele'
)])

relationship_selectors = collections.OrderedDict(((x.name, x) for x in [
    ItemSelection('gene_relationship_type', elem_attr('Type'), '1', 'Type of relationship between variant and gene'),
    ItemSelection('symbol', xpath_one_string('Symbol/ElementValue[@Type="Preferred"]/text( )'), '1', 'Related gene symbol'),
]))

def clinvar_report_to_objects(xml_input):
    for elem in iterate_by_tag('ClinVarSet', xml_input):
        # TODO- what about Genotype?
        # TODO- what about 'asserted, but not computed?'
        output = collections.OrderedDict()
        for attrib, selector in varset_selectors.items():
            output[attrib] = selector.selector(elem)
        # I'm actually not sure if items that are not "current" even show up in the clinvar report
        if not output['clinvarset_record_status'] in ('current', b'current'):
            continue
        # locations = []
        # for loc in find_locations(rcv):
        #    locations.append(dict( [(attrib, selector(loc)) for (attrib, selector) in location_selectors.items()]))
        output['locations'] = [
            collections.OrderedDict(( (attrib, selector(loc)) for (attrib, selector) in location_selectors.items())) for loc in find_locations(elem)
        ]
        output['variant_relationships'] = [
            collections.OrderedDict(( (attrib, selector.selector(rel)) for (attrib, selector) in relationship_selectors.items())) for rel in find_measure_relationships(elem)
        ]
        # scvs = []
        # for scv in find_scvs(elem):
        #     scvs.append(dict([(attrib, selector(scv)) for (attrib, selector) in scv_selectors.items()]))
        if len(output['variant_relationships']) != 0:
            log.info('Found some variant relationships')
        output['scv'] = [
            collections.OrderedDict(( (attrib, selector.selector(scv)) for (attrib, selector) in scv_selectors.items() )) for scv in find_scvs(elem)
        ]
        yield output


def aggregate_scv_significance(clinvarset):
    included_scv_significance = collections.OrderedDict()
    for scv in clinvarset['scv']:
        for comment in scv['significance_comment']:
            if comment.startswith('Converted during submission to'):
                # how's this for hacky? Obviously will break if ClinVar changes or is not consistent...
                scv['original_significance'] = scv['significance']
                scv['significance'] = comment[31:-1]
                break
        if not scv['review_status'] in excluded_scv_review_statuses:
            included_scv_significance[scv['accession']] = scv['significance']
    clinvarset['clinvar_asserted_pathogenic'] = 1 if 'Pathogenic' in included_scv_significance.values() or 'Likely pathogenic' in included_scv_significance.values() else 0
    if len(included_scv_significance) > 0:
        clinvarset['scv_with_criteria'] = included_scv_significance
    return clinvarset


def normalize_locations(clinvarset, normalizers):
    locations = [process_sequence_location(loc, normalizers) for loc in clinvarset['locations'] if loc['Assembly'] in normalizers]
    clinvarset['locations'] = [loc for loc in locations if loc is not None]
    return clinvarset


def remove_empties(clinvarset):
    clinvarset = collections.OrderedDict((
        (k, v) for (k, v) in clinvarset.items() if v is not None and (not hasattr(v, '__len__') or len(v) != 0)
    ))
    clinvarset['scv'] = [ collections.OrderedDict((
        (k, v) for (k, v) in scv.items() if v is not None and (not hasattr(v, '__len__') or len(v) != 0)
    )) for scv in clinvarset['scv'] ]
    return clinvarset


# FIXME: urllib.quote would be faster, but not as flexible in terms of characters to encode
def vcf_safe_encode(data, to_quote=' :;=%,\r\n\t', exclude='', extra=''):
    '''Replace special characters (as defined in the VCF spec) with "capitalized percent encoding"

    The VCF format uses certain printable characters as having special meaning:
    : ; = % , <CR> <LF> <TAB>

    <SPACE> used to be a special character disallowed in INFO fields, but is OK
    now as of VCFv4.3 spec, and arguably should have been since VCFv4.2 with <TAB>
    as the only allowable field separator. But htsjdk and programs that use htsjdk
    do not support v4.3 yet...

    The pipe character ("|") also has special meaning in some INFO fields, so
    it may need to be similarly escaped sometimes.
    '''
    to_quote += extra
    if exclude:
        to_quote = [x for x in to_quote if not x in exclude]
    quote_map = dict(( (c, '%{:02X}'.format(ord(c))) for c in to_quote))
    if not isinstance(data, list):
        data = [data]
    return ','.join(''.join(quote_map.get(c, c) for c in str(txt)) for txt in data)


def scv_to_string(scv):
    return '|'.join( (vcf_safe_encode(scv.get(sel, ''), extra='|') for sel in scv_selectors if sel not in exclude_from_vcf_scv) )


def varset_to_dict(clinvarset):
    res = collections.OrderedDict((
        (sel, vcf_safe_encode(clinvarset.get(sel, ''))) for sel in varset_selectors
    ))
    res['locations'] = ','.join(map(str, clinvarset['locations']))
    res['clinvar_scv'] = ','.join( (scv_to_string(scv) for scv in clinvarset['scv']) )
    if clinvarset['scv_with_criteria']:
        res['scv_with_criteria'] = ','.join(('%s|%s'%(k,vcf_safe_encode(v)) for (k,v) in clinvarset['scv_with_criteria'].items()))
    return res


def varset_to_vcf_data(clinvarset):
    '''Create one or more tuples of ((chrom, pos, ref, alt), info_fields_dict) from a processed ClinVarSet
    '''
    record = varset_to_dict(clinvarset)
    del record['locations'] # will handle this separately...
    locations = clinvarset['locations']
    for loc in locations:
        if loc:
            if loc.ref == loc.alt:
                log.warning('Unable to represent %s in VCF (VCF cannot represent "reference" alleles)', loc)
            yield ((loc.contig, loc.pos, loc.ref, loc.alt), record)


class VCFRecordContainer(dict):
    def __init__(self):
        self.records = {}

    def add_record(self, partial_vcf_record):
        (chrom_pos_ref_alt, info) = partial_vcf_record
        stored_info = self.records.setdefault(chrom_pos_ref_alt, info)
        if id(stored_info) != id(info):
            for (k,v) in info.items():
                if k in stored_info:
                    if v != stored_info[k]:
                        stored_info[k] += ',' + v
                else:
                    stored_info[k] = v


if '__main__' == __name__:
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--assembly',
                        help="Genomic reference assembly (one of GRCh37 or GRCh38 for now)",
                        metavar='ASSEMBLY',
                        action='append',
                        choices=('GRCh37', 'GRCh38'))
    parser.add_argument('-r', '--reference',
                        help='FASTA sequence file for reference (used for normaliztion), by default will be determined from reference',
                        action='append',
                        metavar='REFERENCE_FASTA')
    parser.add_argument('-v', '--verbosity',
                        action='count',
                        help='increase logging verbosity')
    parser.add_argument('-i', '--input',
                        help='input ClinVarFull xml file (optionally gzip compressed).',
                        metavar='INPUT')
    parser.add_argument('-f', '--format',
                        help='format of output',
                        choices=('yaml', 'tsv', 'vcf', 'jsonl'))
    args = parser.parse_args()

    if 'yaml' == args.format:
        # import here (instead of at top of file) so we only do so if yaml requested (in case it's not installed, for instance)
        import yaml
        try:
            from yaml import CLoader as Loader, CDumper as Dumper
        except ImportError:
            from yaml import Loader, Dumper
        Dumper.add_representer(collections.OrderedDict,
            lambda dumper, data: dumper.represent_dict(data.items()))
        Dumper.add_representer(Variant,
            lambda dumper, variant: dumper.represent_dict(variant._asdict().items()))


    try:
        log.setLevel((logging.CRITICAL, logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG)[args.verbosity])
    except:
        log.setLevel(logging.CRITICAL)

    if args.input:
        if args.input.endswith('.gz'):
            xml_input = gzip.open(args.input, 'r')
        else:
            xml_input = open(args.input, 'r')
    else:
        xml_input = sys.stdin

    if args.assembly is None:
        args.assembly = ['GRCh37', 'GRCh38']

    if args.reference is not None:
        if len(args.reference) != len(args.assembly):
            raise ValueError('The number of FASTA files, if given, must match the number of reference assemblies')
        for ref, fasta in zip(args.reference, args.reference):
            fastas[ref] = fasta
    normalizers = collections.OrderedDict()
    for ref in args.assembly:
        try:
            normalizers[ref] = Normalizer(fastas[ref])
        except:
            normalizers[ref] = None
            log.warning('Unable to open fasta to correspond with %s', ref)

    if args.format == 'vcf':
        print("##fileformat=VCFv4.2")
        for name, selector in varset_selectors.items():
            print('##INFO=<ID=%s,Number=%s,Type=String,Description="%s">'%(name,
                selector.number, selector.description))
        print(scv_info_header)
        print("\t".join('#CHROM POS ID REF ALT QUAL FILTER INFO'.split()))
        vcf_records = VCFRecordContainer()

    for clinvarset in clinvar_report_to_objects(xml_input):
        clinvarset = aggregate_scv_significance(clinvarset)
        clinvarset = normalize_locations(clinvarset, normalizers)
        if args.format == 'yaml':
            print('---')
            print(yaml.dump(remove_empties(clinvarset), Dumper=Dumper, default_flow_style=False))
        elif args.format == 'jsonl':
            print(json.dumps(remove_empties(clinvarset)))
        elif args.format == 'tsv':
            print("\t".join(map(str, varset_to_dict(clinvarset).values())))
        elif args.format == 'vcf':
            for record in varset_to_vcf_data(clinvarset):
                vcf_records.add_record(record)
        else:
            raise ValueError("format %s is not yet supported"%args.format)

    if args.format == 'vcf':
        for locvar in sorted(vcf_records.records.keys()):
            info = vcf_records.records[locvar]
            # special handling to really aggregate asserted_pathogenic, since that's the whole point...
            info['clinvar_asserted_pathogenic'] = '1' if '1' in info['clinvar_asserted_pathogenic'].split(',') else '0'
            print("\t".join((locvar[0], str(locvar[1]), '.', locvar[2], '.' if locvar[2] == locvar[3] else locvar[3], '.', '.',
                ';'.join(('%s=%s'%(k,v) for (k,v) in info.items())))))
