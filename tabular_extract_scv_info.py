#!/usr/bin/env python

from __future__ import print_function

import sys
import gzip
import yaml
from xml_iterate import iterate_by_tag
from variant_normalizer import Normalizer
from collections import namedtuple, OrderedDict
import lxml.etree as etree
import logging

log = logging.getLogger('clinvar-extractor')
log.addHandler(logging.StreamHandler())

included_scv_review_statuses = (
    'reviewed by expert panel',
    'practice guideline',
    'criteria provided, single submitter')

excluded_scv_review_statuses = (
    'criteria provided, conflicting interpretations', # that's just plain weird for an SCV...
    'no assertion criteria provided',
    'no assertion provided',
    ''
)

try:
        from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
        from yaml import Loader, Dumper

Variant = namedtuple('Variant', 'contig pos ref alt')


try:
    normalizers = {
        'GRCh37': Normalizer('GCF_000001405.25_GRCh37.p13_genomic.fna'),
        'GRCh38': Normalizer('GCF_000001405.26_GRCh38_genomic.fna')
    }
except:
    normalizers = {}


def one_text(elem, qname):
    found = elem.findall(qname)
    if found is None:
        return ''
    if len(found) > 1:
        log.error("Too many items for %s in %s", qname, elem)
    return found[0].text


def variant_from_fields(loc, accession, position, ref, alt):
    v = Variant(*(loc.get(x) for x in (accession, position, ref, alt)))
    if any (x is None for x in v):
        return None
    normalizer = normalizers.get(loc.get('Assembly'))
    if normalizer is None:
        log.info("Unable to find normalizer for assembly [%s] location %s", loc.get("Assembly"), etree.tostring(loc).rstrip())
        return v
    pos, ref, alts = normalizer.normalize(*v)
    return Variant(v.contig, pos, ref, alts[0])


def process_sequence_location(loc):
    #parentType = loc.getparent().get("Type")
    vcf_variant = variant_from_fields(loc, 'Accession', 'positionVCF', 'referenceAlleleVCF', 'alternateAlleleVCF')
    old_style_variant = variant_from_fields(loc, 'Accession', 'start', 'referenceAllele', 'alternateAllele')

    if old_style_variant and vcf_variant and old_style_variant != vcf_variant:
        log.warning("both vcf_variant and old_style_variant provided, but do not normalize to the same!\n  %s\n  %s != %s",
                  (etree.tostring(loc).rstrip(), vcf_variant, old_style_variant))
        return None
    if vcf_variant:
        return vcf_variant
    elif old_style_variant:
        log.info("No vcf variant, so used %s", str(old_style_variant))
        return old_style_variant
    else:
        log.warning("Not enough info to process location: %s", etree.tostring(loc).rstrip())
        return None


#for elem in iterate_by_tag('ClinVarSet', gzip.open(sys.argv[1])):
for elem in iterate_by_tag('ClinVarSet', sys.stdin):
    if not 'current' == one_text(elem, 'RecordStatus'):
        continue
    output = OrderedDict()
    rcv = elem.find('./ReferenceClinVarAssertion')
    # TODO- what about Genotype?
    # TODO- what about 'asserted, but not computed?'
    output['clinvarset_id'] = elem.get('ID')
    output['title'] = one_text(elem, 'Title')
    locations = [process_sequence_location(loc) for loc in rcv.xpath('./MeasureSet[@Type="Variant"]/Measure/SequenceLocation')]
    output['locations'] = '|'.join(['%s:%s:%s:%s'%v for v in locations if v is not None])
    #output['locations'] = [dict(loc._asdict()) for loc in locations if loc is not None]
    acc = rcv.find('ClinVarAccession') # {1,1}
    rcv_clinsig = rcv.find('ClinicalSignificance') # {0,1}
    # FIXME - maybe skip if no ClinicalSignificance?
    if rcv_clinsig is None:
        continue
    output['rcv'] = "%s.%s"%(acc.get('Acc'), acc.get('Version', '0'))
    output['rcv_significance'] = rcv_clinsig.findtext('Description') or ''
    output['rcv_explanation'] = rcv_clinsig.findtext('Explanation') or '' # {0,1}
    output['rcv_review_status'] = rcv_clinsig.findtext('ReviewStatus') or '' # {0,1}
    output['rcv_date_created'] = rcv.get('DateCreated')
    output['rcv_date_updated'] = rcv.get('DateLastUpdated')
    output['rcv_date_last_evaluated'] = rcv_clinsig.get('DateLastEvaluated', '')
    scvs = []
    included_scv_significance = []
    for scv in elem.findall('ClinVarAssertion'):
        sacc = scv.find('ClinVarAccession')
        scv_clinsig = scv.find('ClinicalSignificance')
        if scv_clinsig is None:
            continue
        significance = scv_clinsig.findtext('Description')
        original_significance = significance
        for comment in scv_clinsig.findall('Comment'):
            if comment.text.startswith('Converted during submission to'):
                # how's this for hacky? Obviously will break if ClinVar changes or is not consistent...
                significance = comment.text[31:-1]
                break
        review_status = scv_clinsig.findtext('ReviewStatus') or ''
        if not review_status in excluded_scv_review_statuses:
            included_scv_significance.append(significance)
        # scv = {
        #     'accession': '%s.%s'%(sacc.get('Acc'), sacc.get('Version', '0')),
        #     'review_status': scv_clinsig.findtext('ReviewStatus') or '',
        #     'significance': significance,
        #     'original_significance': original_significance,
        #     'date_updated': scv.get('DateUpdated', '')
        #     }
        # scv = dict((k,v) for (k,v) in scv.iteritems() if '' != v)
        scv = '|'.join((
            '%s.%s'%(sacc.get('Acc'), sacc.get('Version', '0')),
            scv_clinsig.findtext('ReviewStatus') or '',
            significance or '',
            original_significance or '',
            scv.get('DateUpdated', '')))
        scvs.append(scv)
    output['scv'] = '^'.join(scvs)
    output['scv_critsig'] = '|'.join(included_scv_significance)
    output['asserted_pathogenic'] = '1' if 'Pathogenic' in included_scv_significance or 'Likely pathogenic' in included_scv_significance else '0'
    #print(json.dumps(output, indent=2))
    #print('---')
    #print(yaml.dump(output, Dumper=Dumper, default_flow_style=False))
    print("\t".join((x.encode('utf-8', 'xmlcharrefreplace') for x in output.values())))
