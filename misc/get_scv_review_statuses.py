#!/usr/bin/env python

from __future__ import print_function

import sys
import gzip
from xml_iterate import iterate_by_tag
from collections import defaultdict


def one_text(elem, qname):
    found = elem.findall(qname)
    if found is None:
        return ''
    if len(found) > 1:
        log.error("Too many items for %s in %s", qname, elem)
    return found[0].text

review_status_counts = defaultdict(int)

#for elem in iterate_by_tag('ClinVarSet', gzip.open('ClinVarFullRelease_2017-12.xml.gz')):
for elem in iterate_by_tag('ClinVarSet', sys.stdin):
    if not 'current' == one_text(elem, 'RecordStatus'):
        continue
    for status in elem.findall('ClinVarAssertion/ClinicalSignificance/ReviewStatus'):
	review_status_counts[status.text] += 1

print('\n'.join(['%s\t%d'%(k,v) for (k,v) in review_status_counts.items()]))
