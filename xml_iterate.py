#!/usr/bin/env python

import lxml.etree as etree


def iterate_by_tag(tag, source, **kwargs):
    """
    https://stackoverflow.com/questions/7171140/using-python-iterparse-for-large-xml-files
    http://lxml.de/parsing.html#modifying-the-tree
    Based on Liza Daly's fast_iter
    http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    See also http://effbot.org/zone/element-iterparse.htm
    """
    context = etree.iterparse(source, tag=tag, **kwargs)
    for event, elem in context:
        yield elem
        # It's safe to call clear() here because no descendants will be
        # accessed
        elem.clear()
        # Also eliminate now-empty references from the root node to elem
        for ancestor in elem.xpath('ancestor-or-self::*'):
            while ancestor.getprevious() is not None:
                del ancestor.getparent()[0]
    del context


